#!/bin/bash

# This script boot up dependencies
# for local development. These services
# are exposed on 0.0.0.0 with the respective
# port as mentioned in the file:
#
#  docker-compose-public-ports.yml
#
# Requires docker-compose to be installed
# and available within $PATH variable.

_BASE="docker-compose.yml"
_PORT="docker-compose-public-ports.yml"
_SCRIPT="$(cd "$(dirname "$0")" ; pwd)"
_YML_FILES="$(dirname ${_SCRIPT})"
WAIT_TIMEOUT=89
INITKEYCLOAK=0

cd "${_YML_FILES}"
if [ ! -f "${_BASE}" -a ! -f "${_PORT}" ]; then
  echo "Could not find required files ${_BASE} or ${_PORT} in directory ${_YML_FILES}, required to continue."
  exit 1
fi

which docker-compose > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
  echo "Could not find docker-compose in ${PATH}, required to continue."
  exit 1
fi

which docker > /dev/null 2> /dev/null
if [ $? -ne 0 ]; then
  echo "Could not find docker in ${PATH}, required to continue."
  exit 1
fi

docker-compose down
if [ $? -ne 0 ]; then
  echo "docker-compose returned a non zero exit code when attempting to close containers."
  echo "Please forcefully close containers and attempt to re-run this script."
  exit 1
fi


CIR="-\|/"
WAITCNT=0
function circ() {
	echo -e -n '\b'
	echo -n "${CIR:$WAITCNT:1}"
	WAITCNT=`expr $WAITCNT + 1`
	[ $WAITCNT -ge ${#CIR} ] && WAITCNT=0
}

function wait_circ() {
	circ
	sleep 0.2
	circ
	sleep 0.2
	circ
	sleep 0.2
	circ
	sleep 0.2
	circ
	sleep 0.2
}

function wait_for_log()
{
	local SERVICE=$1
	local SEARCHSTR="$2"
	local COUNT=${3:-1}
	local i=$WAIT_TIMEOUT
	local NOTFOUND=1
	echo -n polling $SERVICE for \"$SEARCHSTR\" \($COUNT occurencies\)...'   '
	docker-compose logs $SERVICE >/dev/null 2>&1 || { echo Error: No such service: $SERVICE; exit 1; }
	while [ $i -gt 0 -a $NOTFOUND -eq 1 ] ; do
		[ $i -lt $WAIT_TIMEOUT ] && wait_circ
		foundnum=$(docker-compose logs $SERVICE 2>&1 | grep -m $COUNT -c "$SEARCHSTR")
		[ $foundnum -ge $COUNT ] && NOTFOUND=0
		i=$(expr $i - 1)
	done
	if [ $NOTFOUND -eq 1 ]; then
		echo Timeout waiting. Giving up.
		docker ps -a
		docker-compose logs $SERVICE
		docker-compose down --rmi local
		exit 1
	fi
	echo -e '\bok'
}

if [ "$1" == "-test" ]; then
	echo "make test: starting up docker-compose environment."
	TST=1
else
	echo "DEV: starting up docker-compose environment and exposing ports."
	export COMPOSE_FILE="${_BASE}:${_PORT}"
fi

docker-compose up -d postgres

wait_for_log postgres "database system is ready to accept connections" 2

echo -e `basename "$0"` done.
